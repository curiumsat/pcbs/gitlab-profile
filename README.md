# Curium PCBs

Located here are all open-source PCBs relevant for the Curium satellite Bus. Always the newest [KiCAD EDA](https://www.kicad.org/) shall be used for PCB development. For all new PCBs the [LibreCube board specification](https://librecube.gitlab.io/standards/board_specification/) shall be used.

Most important is the main PCB which contains OBC, ADCS and EPS subsystems:

![obc-1](image.png){width=25%}

![obc-2](image-1.png){width=25%}

The Satnogs Comms adapter board provides a interface between main and Satnogs-comms:

![adapter-board](image-3.png){width=25%}

The PCB stack inside Curium:

![pcb-stack](image-2.png){width=25%}

[Satnogs-Comms](https://libre.space/projects/satnogs-comms/) is used for communications:

![satnogs-comms](https://i0.wp.com/libre.space/wp-content/uploads/2020/09/PXL_20230623_095349203-scaled.jpg?resize%253D1536%252C2048%2526ssl%253D1){width=25%}

Software updates are possible from outside through a Tag-connect TC2050-IDC connector for multiple subsystems and also allows access to the CAN-bus:

![flash-pcb](image-4.png){width=25%}

For a 12U configuration a 12U adapter board is needed to connect to one of the solar panels.
